libez
=====

`libez` is a small C libray that provides some higher level niceties.

See the header files in the repo for usage details.

# building

    make
    make install

