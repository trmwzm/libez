#ifndef EZ_FILE_HEADER_FILE
#define EZ_FILE_HEADER_FILE

#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include "srv.h"
#include "path.h"

typedef bool (*dirlistFn)(char *path, struct dirent *entry, void *data);
bool dirlist(char *path, bool recursive, dirlistFn cb, void *data);
bool fileisdir(char *path);
long fileexists(char *path);
bool filecopy(char *path, char *newPath, mode_t mode);
const char *fileext(const char *name);

#endif
