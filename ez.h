#ifndef EZ_HEADER_FILE

#ifdef __cplusplus
extern "C" {
#endif

#include "func.h"
#include "dict.h"
#include "sized_ptr.h"
#include "array.h"
#include "object.h"

#include "shell.h"
#include "path.h"
#include "file.h"
#include "srv.h"
#include "http.h"
#include "linenoise.h"

#include "test.h"

#ifndef __cplusplus
#include "sds.h"
#else //__cplusplus/sds
// This allows sds strings to be manipulated in C++
// something about the sds header doesn't agree with C++
typedef char* sds;
struct sdshdr {
    int len;
    int free;
    char buf[];
};
void sdsfree(sds s);
size_t sdslen(sds s);
#endif // __cplusplus/sds

#ifdef __cplusplus
}
#endif


#endif
