#include "sized_ptr.h"

void *alloc(index_t n, index_t size){
    void *d = calloc(n ? n : 1, size + sizeof(size_hdr));
    ((size_hdr*)d)->size = size;
    ((size_hdr*)d)->length = n;
    return d + sizeof(size_hdr);
}

void *extend(void *ptr, index_t n){
    size_hdr *hdr = size_header(ptr);
    hdr->length += n;
    return realloc(hdr, (hdr->size * hdr->length) + sizeof(size_hdr)) + sizeof(size_hdr);
}

size_hdr *size_header(void *ptr){
    return ((size_hdr*)(ptr - sizeof(size_hdr)));
}

index_t overload size(void *ptr){
    return size_header(ptr)->size;
}

index_t overload length(void *ptr){
    return size_header(ptr)->length;
}

void overload release(void *d){
    free(d - sizeof(size_hdr));
}

bool check_size(void *ptr, index_t s){
    return size(ptr) > s;
}

bool check_length(void *ptr, index_t c){
    return length(ptr) > c;
}
