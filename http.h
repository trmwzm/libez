#ifndef EZ_HTTP_HEADER_FILE
#define EZ_HTTP_HEADER_FILE

#include <stdbool.h>
#include "srv.h"
#include "sds.h"

sds httphdr(char *reqtyp, char *url, sds *fields);
bool httpreq(int sd, char *reqtyp, char *url, sds *fields, char *data, size_t len);
bool httpget(int sd, char *url);

typedef struct HTTPRes {
    sds header;
    sds body;
} HTTPr;
HTTPr httpread(int sd);



#endif
