#ifndef EZ_OBJECT_FILE_HEADER
#define EZ_OBJECT_FILE_HEADER

#include "dict.h"
#include "array.h"
#include "sds.h"

enum object_types {
    OBJ_INT,
    OBJ_FLOAT,
    OBJ_STR,
    OBJ_ARR,
    OBJ_DICT,
    OBJ_PTR,
    OBJ_FUNC,
    OBJ_ERR
};

typedef struct object_t {
    enum object_types type;
    int subtype;
    int array_length;
    union {
        void *ptr;
        long d;
        float f;
        dict *dict;
        array *arr;
        char *str;
    };
} object_t;

object_t *object_new(enum object_types type);
void object_dealloc(object_t *obj, void (*fn)(void*));
void object_free(object_t *obj, void (*fn)(void*));
object_t *object_from_str(char *str);
sds object_to_str(object_t *obj);
void object_set_int(object_t *obj, long val);
void object_set_float(object_t *obj, float val);
void object_set_ptr(object_t *obj, void *val);
void object_set_str(object_t *obj, char *val);
void object_set_dict(object_t *obj, dict *val);
void object_set_array(object_t *obj, array *val);

#endif
