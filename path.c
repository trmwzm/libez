#include "path.h"
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

sds pathjoin(char *base, ...){
    va_list argp;
    char *p;
    sds out = sdsnew(base);

    va_start(argp, base);

    while ((p = va_arg(argp, char *)) != NULL){
        if (out[sdslen(out)-1] != '/' && p[0] != '/'){
            out = sdscat(out, "/");
        }
        out = sdscat(out, p);
    }

    return out;
}

sds *pathsplit(char *path, int *count){
    return sdssplitlen(path, strlen(path), "/", 1, count);
}

// make path recursively
bool pathmk(const char *name){
    int pid, status=-1;

    if ((pid=fork()) >= 0){
        if (pid == 0){
            exit(execl ("/bin/mkdir", "/bin/mkdir", "-p", name, NULL));
        } else {
            wait(&status);
        }
    } else {
        return false;
    }

    return  status == 0;
}

bool pathrm(char *name){
    int pid, status=-1;

    if ((pid=fork()) >= 0){
        if (pid == 0){
            exit(execl ("/bin/rm", "/bin/rm", "-rf", name, NULL));
        } else {
            wait(&status);
        }
    } else {
        return false;
    }

    return  status == 0;
}

