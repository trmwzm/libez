EZ=sds.c srv.c dict.c object.c array.c sized_ptr.c file.c shell.c path.c http.c linenoise.c
EZ_HDR=sds.h dict.h srv.h object.h array.h sized_ptr.h func.h ez.h test.h file.h shell.h path.h http.h linenoise.h
EZ_OBJ=$(EZ:.c=.o)

CFLAGS=-I/usr/local/include
LDFLAGS=-L/usr/local/lib
suffix_Darwin=dylib
suffix?=$(suffix_$(shell uname))
ifeq ($(suffix)X,X)
	suffix=so
endif
dest=/usr/local

all: ez

ez:
	$(CC) -Wall -Wno-unused-function -std=c99 -c -fpic $(EZ)  $(CFLAGS)
	ar rcs libez.a $(EZ_OBJ)
	$(CC) -std=c99 -shared -o libez.$(suffix) $(EZ_OBJ) $(LDFLAGS)

.PHONY: test
test: ez
	$(CC) -g -std=c99 -o test test.c libez.a $(CFLAGS) $(LDFLAGS)

install:
	cp libez.* $(dest)/lib/
	mkdir -p $(dest)/include/ez
	cp $(EZ_HDR) $(dest)/include/ez

uninstall:
	rm -rf $(dest)/include/ez $(dest)/lib/libez.*

clean:
	rm -f *.o libez.*
