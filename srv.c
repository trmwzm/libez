#include "srv.h"

#include <string.h>
#include <unistd.h>
#include <regex.h>

#ifdef __linux__
#include <sys/sendfile.h>
#endif

struct addrinfo *srvinfo(char *url, char *port){

    char *host;
    int count;
    struct addrinfo hints, *info;
    sds *s;

    if (strncmp(url, "http:", 5) == 0){
        url = url+7;
    } else if (strncmp(url, "https:", 6) == 0){
        url = url +8;
    }

    s = sdssplitlen(url, strlen(url), "/", 1, &count);
    host = count >= 1 ? s[0] : url;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if (getaddrinfo(host, port ? port : "80", &hints, &info) != 0) {
        sdsfreesplitres(s, count);
        return NULL;
    }
    sdsfreesplitres(s, count);

    return info;
}

int srvconnect(struct addrinfo *info){
    struct addrinfo *i;
    int sd = -1;

    for(i = info; i != NULL; i = i->ai_next){
        if ((sd = socket(i->ai_family, i->ai_socktype, i->ai_protocol)) == -1) {
            continue;
        }

        if (connect(sd, i->ai_addr, i->ai_addrlen) == -1) {
            close(sd);
            continue;
        }

        break;
    }

    if (i == NULL)
        return -1;

    return sd;
}

int srvaccept(int sd){
    struct sockaddr_storage addr;
    socklen_t sin_size = sizeof(addr);
    return accept(sd, (struct sockaddr *)&addr, &sin_size);
}

sds readall(int cli){
    long long n = 0;
    char buf[BUFSZ];
    sds out = NULL;

    while((n = recv(cli, (void*)buf, BUFSZ, 0)) > 0){
        if (out == NULL){
            out = sdsnewlen(buf, n);
        } else {
            out = sdscatlen(out, buf, n);

        }
    }

    return out;
}

sds readuntil(int cli, char *rgx){
    long long n = 0;
    regex_t reg;
    sds out = NULL;

    if (regcomp(&reg, rgx, REG_EXTENDED))
        return out;

    char buf[BUFSZ];

    while((n = recv(cli, buf, BUFSZ, 0)) > 0 &&
            regexec(&reg, out, 0, NULL, 0) != 0){
        if (out == NULL){
            out = sdsnewlen(buf, n);
        } else {
            out = sdscatlen(out, buf, n);

        }
    }

    regfree(&reg);
    return out;
}

int srvlisten(char *ip, char *port, int num){
    int sd = -1, yes = 1;
    addrinfo hints, *info, *p;
    memset(&hints, 0, sizeof hints);

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(ip ? ip : "127.0.0.1", port, &hints, &info) != 0) {
        return -1;
    }

    for(p = info; p != NULL; p = p->ai_next) {
        if ((sd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            continue;
        }

        if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) == -1) {
            continue;
        }

        if (bind(sd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sd);
            continue;
        }

        break;
    }

    if (p == NULL || listen(sd, num) == -1){
        close(sd);
        return -1;
    }

    freeaddrinfo(info);

    return sd;
}

