#include "array.h"
#include <stdio.h>

index_t arrayLength(array *arr){
    return arr->last;
}

array *arrayCreate(index_t len, index_t sz){
    array *arr = malloc(sizeof(array));
    arr->ptr = alloc(len ? len : array_default_incr, sz);
    arr->last = 0;
    return arr;
}

void arrayFree(array *arr, void(*free_fn)(void *)){
    if (!arr)
        return;

    if (free_fn){
        for(index_t i = 0; i < arr->last; i++){
            free_fn(((void**)arr->ptr)[i]);
        }
    }

    if (arr->ptr)
        release(arr->ptr);

    free(arr);
}

void *arrayGet(array *arr, index_t index){
    if (index > arr->last)
        return NULL;

    return arr->ptr[index];
}

void *arrayPop(array *arr){
    if (arr->last == 0){
        return NULL;
    }

    void *item = arrayGet(arr, arr->last-1);
    arr->ptr[arr->last-1] = NULL;
    arr->last -= 1;

    if (length(arr->ptr) - arr->last > array_default_incr){
        arr->ptr = extend(arr->ptr, -array_default_incr);
    }

    return item;
}

void *arrayRemove(array *arr, index_t index){
    if (arr->last == 0 || index >= arr->last){
        return NULL;
    }

    void *item = arrayGet(arr, index);
    for(index_t i = index; i < arrayLength(arr)-1; i++){
        arr->ptr[i] = arr->ptr[i+1];
    }
    arr->last -= 1;

    if (length(arr->ptr) - arr->last > array_default_incr){
        arr->ptr = extend(arr->ptr, -array_default_incr);
    }

    return item;
}

void arraySet(array *arr, index_t index, void *item){
    if (arr->last < index){
        return;
    }

    arr->ptr[index] = item;
}

array *arrayPush(array *arr, void *item){
    if (arr->last >= length(arr->ptr)){
        arr->ptr = extend(arr->ptr, array_default_incr);
    }

    arraySet(arr, arr->last, item);
    arr->last += 1;

    return arr;
}

array *arrayMap(array *arr, arrayMapFn fn){
    index_t sz = arrayLength(arr);
    for(index_t i = 0; i < sz; i++){
        fn(arr, i, arrayGet(arr, i));
    }

    return arr;
}

void *arrayReduce(array *arr, arrayFilterFn fn, void *result){
    index_t sz = arrayLength(arr);
    for(index_t i = 0; i < sz; i++){
        fn(arr, i, arrayGet(arr, i), result);
    }

    return result;
}

