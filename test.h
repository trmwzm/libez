#ifndef EZ_TEST_HEADER_FILE
#define EZ_TEST_HEADER_FILE

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct testSuite {
    size_t passes, fails;
    const char *casename;
    void *data;
} testSuite;

typedef enum testStatus {
    test_pass,
    test_fail,
} testStatus;

#define testSuiteNew { 0, 0, NULL, NULL }

#define testAssert(assertion) if (!(assertion)){ \
    fprintf(stderr, ANSI_COLOR_RED "%s:%d failed assertion" ANSI_COLOR_RESET " %s\n", __FILE__, __LINE__, #assertion);\
    goto Fail; \
}

#define testCase(ts, name, func) \
    (ts).casename = name; \
    if (func(&ts) == test_fail){ \
        (ts).fails += 1; \
        fprintf(stderr, ANSI_COLOR_RED "%s:%d - FAIL %s\n" ANSI_COLOR_RESET , __FILE__, __LINE__, ts.casename); \
    } else { \
        (ts).passes += 1; \
    }

#define testFinish(ts) \
    fprintf(stderr, ANSI_COLOR_CYAN"TOTAL: %zd\n"ANSI_COLOR_RESET, (ts).passes + (ts.fails)); \
    fprintf(stderr, (ts).fails == 0 ? ANSI_COLOR_GREEN "PASSED (%02f%%)\n" ANSI_COLOR_RESET : ANSI_COLOR_RED "FAILED (%02f%%)\n" ANSI_COLOR_RESET, \
                (((float)(ts).passes / (float)((ts).passes + (ts).fails)) * 100.0))

#define test_end_default return test_pass; Fail: return test_fail
#endif
