#ifndef EZ_SRV_HEADER_FILE
#define EZ_SRV_HEADER_FILE


// Default buffer size
#define BUFSZ 2048

#include "dict.h"
#include "sds.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

typedef struct addrinfo addrinfo;
struct addrinfo *srvinfo(char *url, char *port);
int srvconnect(struct addrinfo *info);
int srvaccept(int sd);
sds readall(int cli);
sds readuntil(int cli, char *rgx);
int srvlisten(char *ip, char *port, int num);

int _endswith(char *a, size_t alen, char *b, size_t blen);
#endif
