#ifndef EZ_FUNC_HEADER_FILE
#define EZ_FUNC_HEADER_FILE

// the "call" operator
#define func_call(n, ...) n(__VA_ARGS__)

#ifdef __llvm__
#define overload __attribute__((overloadable))
#else
#define overload
#endif


//typedef void (*item_func)(void *item);
//void *map(void *ptr, item_func fn);

#endif
