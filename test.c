#include "ez.h"
#include <string.h>

testStatus testArrayCreate(testSuite *t){
    t->data = arrayCreate(0, sizeof(int));

    testAssert(t != NULL);
    testAssert(arrayLength(t->data) == 0);
    testAssert(length(((array*)t->data)->ptr) == array_default_incr);

    test_end_default;
}

testStatus testArrayPush(testSuite *t){

    for(int i = 0; i < 4500; i++){
        arrayPushVal(((array*)t->data), i, int);
        testAssert(arrayLength(t->data) == i+1);
    }

    test_end_default;
}

testStatus testArrayPop(testSuite *t){

    size_t begin_len = arrayLength(t->data);

    for(int i = 0; i < 250; i++){
        int n = arrayPopVal(((array*)t->data), int);
    }

    testAssert(arrayLength(t->data) == begin_len - 250);

    test_end_default;
}

testStatus testArrayRemove(testSuite *t){

    size_t begin_len = arrayLength(t->data);

    for(int i = 0; i < 250; i++){
        int n = arrayRemoveVal(((array*)t->data), 0, int);
    }

    testAssert(arrayLength(t->data) == begin_len - 250);
    testAssert(arrayGetVal(((array*)t->data), 0, int) == 250);

    test_end_default;
}

static int num_runs = 0;

void add_one_func(array *arr, index_t index, void *obj){
    num_runs += 1;
}

testStatus testArrayMap(testSuite *t){
    arrayMap(t->data, add_one_func);
    testAssert(arrayLength(t->data) == num_runs);

    test_end_default;
}

testStatus testPathJoin(testSuite *t){

    char *a = "this", *b="is", *c="/a/", *d="test";

    sds join = pathjoin(a, b, c, d, NULL);
    testAssert(strcmp(join, "this/is/a/test") == 0);

    return test_pass;

Fail:
    sdsfree(join);
    return test_fail;
}

testStatus testPathMakeRm(testSuite *t){

    testAssert(pathmk("this/is/a/test/"));
    testAssert(fileisdir("this/is/a/test/"));
    testAssert(pathrm("this/is/a/test/"));
    testAssert(!fileisdir("this/is/a/test/"));

    test_end_default;
}

testStatus testFileExists(testSuite *t){
    testAssert(fileexists("test.c") > 0);
    testAssert(fileexists("this_file_doesnt_exist_and_has_a_long_name") < 0);

    test_end_default;
}

testStatus testFileExt(testSuite *t){
    testAssert(strcmp(fileext("test.c"), "c") == 0);
    testAssert(strcmp(fileext("something.something.else"), "else") == 0);
    testAssert(strcmp(fileext("/usr/local.ext1/something.ext2"), "ext2") == 0);
    testAssert(fileext("something.") == NULL);

    test_end_default;
}

testStatus testFileCopy(testSuite *t){
    testAssert(filecopy("test.c", "copy/test.c", 0666));
    testAssert(fileexists("copy/test.c"));
    rmdir("copy");

    test_end_default;
}

int main(int argc, char **argv){
    testSuite t = testSuiteNew;

    testCase(t, "array create", testArrayCreate);
    testCase(t, "array push val", testArrayPush);
    testCase(t, "array map", testArrayMap);
    testCase(t, "array pop val", testArrayPop);
    testCase(t, "array remove val", testArrayRemove);
    // finish array stuff
    arrayFree(t.data, NULL);

    testCase(t, "path join", testPathJoin);
    testCase(t, "pathmk/pathrm", testPathMakeRm);
    testCase(t, "file exists", testFileExists);
    testCase(t, "file ext", testFileExt);
    testCase(t, "file copy", testFileCopy);

    testFinish(t);
}
