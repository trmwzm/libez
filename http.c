#include "http.h"

#include <string.h>
#include <unistd.h>

sds httphdr(char *reqtyp, char *url, sds *fields){
    const char *pth, *host;
    sds *s;
    sds hdr;
    int count;

    if (strncmp(url, "http:", 5) == 0){
        url = url+7;
    } else if (strncmp(url, "https:", 6) == 0){
        url = url +8;
    }

    hdr = sdsnew(NULL);

    s = sdssplitlen(url, strlen(url), "/", 1, &count);
    host = s[0];
    pth = count > 1 ? url+strlen(s[0]) : "/";

    hdr = sdscatprintf(hdr, "%s %s HTTP/1.0\r\nHost: %s\r\n", reqtyp, pth, host);
    for(size_t i = 0; fields && fields[i] != NULL; i++){
        hdr = sdscatprintf(hdr, "%s\r\n", fields[i]);
    }
    hdr = sdscat(hdr, "\r\n");

    sdsfreesplitres(s, count);
    return hdr;
}


bool httpreq(int sd, char *reqtyp, char *url, sds *fields,
             char *data, size_t len){
    sds hdr;
    int v;

    hdr = httphdr(reqtyp, url, fields);

    v = send(sd, hdr, sdslen(hdr), 0);
    if (v == -1){
        goto End;
    }

    if (data && len){
        v = send(sd, data, len, 0);
    }

End:
    sdsfree(hdr);
    return v != -1;
}

bool httpget(int sd, char *url){
    return httpreq(sd, "GET", url, NULL, NULL, 0);
}

// Right now all this does it read from a server (srvconnect)
// and split the response into header and body
HTTPr httpread(int sd){
    HTTPr res;
    int i, j;
    char buf[BUFSZ];

    bool inHeader = true;
    char *bodyStart = NULL;

    i = j = 0;

    res.body = sdsnew(NULL);
    res.header = sdsnew(NULL);
    while((i = read(sd, buf, BUFSZ)) > 0){
        if (inHeader && (bodyStart=strstr(buf, "\r\n\r\n")) != NULL){
            j = i - (bodyStart-buf);
            res.header = sdscatlen(res.header, buf, i-j);
        } else if (bodyStart == NULL){
            res.header = sdscatlen(res.header, buf,i );
        }

        if(bodyStart!=NULL && inHeader){
            res.body = sdscatlen(res.body, bodyStart+4, j-4);
            inHeader = false;
            continue;
        }
        res.body = sdscatlen(res.body, buf, i);
    }

    if (sdscmp(res.header, res.body) == 0){
        sdsclear(res.header);
        return res;
    }

    return res;
}

