#include "file.h"
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>

bool fileisdir(char *path){
    struct stat st;
    if (stat(path, &st) != 0)
        return false;
    return S_ISDIR(st.st_mode);
}

long fileexists(char *path){
    struct stat st;
    if (stat(path, &st) != 0)
        return -1;
    return st.st_size;
}

bool dirlist(char *path, bool recursive, dirlistFn cb, void *data){
    if (path == NULL)
        return false;

    DIR *d;
    struct dirent *entry;
    size_t sz;

    d = opendir(path);
    if (d == NULL)
        return false;

    while((entry = readdir(d)) != NULL){
        if (cb(path, entry, data) == false)
            break;

        if (entry->d_name[0] == '.')
            continue;

        if (recursive && !S_ISDIR(entry->d_type)){
            sz = strlen(path)+strlen(entry->d_name)+3;
            char newPath[sz];
            snprintf(newPath, sz-1, "%s/%s", strlen(path) == 1 && path[0] == '/' ? "" : path, entry->d_name);

            dirlist(newPath, recursive, cb, data);
        }
    }

    closedir(d);
    return true;
}

bool filecopy(char *path, char *newPath, mode_t mode){
    int in, out;
    size_t len;
    bool res = true;

    in = open(path, O_RDONLY);
    if (in < 0)
        return false;

    // if no new path is provided,
    // then just copy it with the same name
    // to the current dir
    if (newPath == NULL)
        newPath = basename(path);

    pathmk(dirname(newPath)); // doesn't matter if it already exists

    out = creat(newPath, mode);
    if (out < 0){
        res = false;
    }

    char buf[BUFSZ];
    while((len = read(in, buf, BUFSZ)) > 0){
        write(out, buf, len);
    }

    close(in);
    close(out);
    return res;
}

const char *fileext(const char *name){
    const char *d = strrchr(name, '.');
    if (!d || d == name || strlen(d) == 1) return NULL;
    return d+1;
}

