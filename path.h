#ifndef EZ_PATH_HEADER_FILE
#define EZ_PATH_HEADER_FILE

// Sometimes PATH_MAX is in weird places,
// it's easier to just define it here
#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

#include <stdbool.h>
#include <errno.h>
#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <strings.h>
#include <stdlib.h>

#include "sds.h"

/* PATH */
sds pathjoin(char *base, ...);
sds *pathsplit(char *path, int *count);
bool pathmk(const char *name);
bool pathrm(char *path);

#endif
