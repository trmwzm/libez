#include "shell.h"

#include <stdlib.h>
#include <stdio.h>
#include "srv.h"

sds shellrun(char *cmd, char *args){
    sds out = sdsnew(NULL); // stdout
    FILE *fp; // proc
    char buf[BUFSZ];
    size_t len = 0;

    fp = popen(cmd, args);
    if (fp == NULL)
        return out;

    while(fgets(buf, BUFSZ+1, fp) != NULL){
        out = sdscatlen(out, buf, len);
    }

    pclose(fp);
    return out;
}
