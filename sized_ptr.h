#ifndef EZ_SIZED_PTR_HEADER_FILE
#define EZ_SIZED_PTR_HEADER_FILE

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "func.h"

#define index_t int64_t

typedef struct size_hdr {
    index_t size;
    index_t length;
} size_hdr;

typedef struct iterator {
    index_t index;
} iterator;

void *alloc(index_t n, index_t size);
void *extend(void *ptr, index_t n);
void overload release(void*);

#define make(t) alloc(1, sizeof(t))
#define set_field(obj, n) (obj)-> n = n

size_hdr *size_header(void *ptr);
index_t overload size(void *ptr);
index_t overload length(void *ptr);

bool check_size(void *ptr, index_t s);
bool check_length(void *ptr, index_t c);

#define assert_size(ptr, s) assert(check_size(ptr, s))
#define assert_length(ptr, c) assert(check_length(ptr, c))

#endif
