#ifndef EZ_ARRAY_HEADER_FILE
#define EZ_ARRAY_HEADER_FILE

#include "sized_ptr.h"

typedef struct array {
    index_t last;
    void **ptr;
} array;


#define array_default_incr 8

index_t arrayLength(array *arr);
array *arrayCreate(index_t len, index_t sz);
void arrayFree(array *arr, void(*free_fn)(void *));
array *arrayExtend(array *a, index_t n);
void *arrayGet(array *arr, index_t index);
array *arrayPush(array *arr, void *item);
void *arrayPop(array *arr);
void *arrayRemove(array *array, index_t index);

#define arraySetVal(arr, index, item, t) \
    if (arr->last >= index){ \
        ((t*)arr->ptr)[index] = item;\
    }

#define arrayPushVal(arr, item, t) \
    if (arr->last >= length(arr->ptr)){ \
        arr->ptr = extend(arr->ptr, array_default_incr); \
    } \
    arraySetVal(arr, arr->last, item, t);\
    arr->last += 1;

#define arrayGetVal(arr, idx, t) \
    ((t*)arr->ptr)[idx]

#define arrayPopVal(arr, t) \
    arrayGetVal(arr, arr->last-1, t); \
    arr->last -= 1;

#define arrayRemoveVal(arr, idx, t) \
    arrayGetVal((arr), idx, t); \
    for(index_t arrayRemoveVal_iter = idx; arrayRemoveVal_iter < arrayLength(arr)-1; arrayRemoveVal_iter++){ \
        ((t*)arr->ptr)[arrayRemoveVal_iter] = ((t*)arr->ptr)[arrayRemoveVal_iter+1]; \
    } \
    arr->last -= 1; \
    if (length(arr->ptr) - arr->last > array_default_incr){ \
        arr->ptr = extend(arr->ptr, -array_default_incr); \
    }

#define arrayForEach(i, a)  for(index_t i = 0; i < arrayLength(a); i++)

typedef void (*arrayMapFn)(array*, index_t, void *);
typedef void (*arrayFilterFn)(array*, index_t, void *, void *);
array *arrayMap(array *arr, arrayMapFn fn);
void *arrayReduce(array *arr, arrayFilterFn fn, void *result);

#endif
