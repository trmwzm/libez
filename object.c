#include "sds.h"
#include "object.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>

object_t *object_new(enum object_types type){
    object_t *obj = malloc(sizeof(object_t));

    obj->type = type;
    switch(type){
    case OBJ_DICT:
        obj->dict = dictCreate(&dictTypeHeapStringCopyKey, NULL);
        break;
    case OBJ_ARR:
        obj->arr = arrayCreate(0, sizeof(object_t));
        break;
    default:
        break;
    }

    return obj;
}

void object_dealloc(object_t *obj, void (*fn)(void*)){
    switch(obj->type){
    case OBJ_DICT:
        dictEmpty(obj->dict, fn);
        break;
    case OBJ_ARR:
        arrayFree(obj->arr, fn);
        break;
    case OBJ_STR:
        sdsfree(obj->str);
        break;
    default:
        if (fn) fn(obj->ptr);
        break;
    }
}

void object_free(object_t *obj, void (*fn)(void*)){
    object_dealloc(obj, fn);
    free(obj);
}

void object_set_int(object_t *obj, long val){
    obj->d = val;
    obj->type = OBJ_INT;
}

void object_set_float(object_t *obj, float val){
    obj->f = val;
    obj->type = OBJ_FLOAT;
}

void object_set_ptr(object_t *obj, void *val){
    obj->ptr = val;
    obj->type = OBJ_PTR;
}

void object_set_str(object_t *obj, char *val){
    obj->str = sdsnew(val);
    obj->type = OBJ_STR;
}

void object_set_dict(object_t *obj, dict *val){
    obj->ptr = val;
    obj->type = OBJ_DICT;
}

void object_set_array(object_t *obj, array *val){
    obj->arr = val;
    obj->type = OBJ_ARR;
}

object_t *object_from_str(char *str){
    long d;
    float f;

    object_t *obj = object_new(OBJ_STR);

    char *end = NULL;

    f = strtof(str, &end);
    if (end != str){
        object_set_float(obj, f);
        return obj;
    }

    d = strtol(str, &end, 10);
    if (end != str){
        object_set_int(obj, d);
        return obj;
    }

    obj->str = strdup(str);
    return obj;
}

sds object_to_str(object_t *obj){
    sds dst = sdsnew(NULL);

    switch(obj->type){
    case OBJ_STR:
        dst = sdscat(dst, obj->str);
        break;
    case OBJ_INT:
        dst = sdscatprintf(dst, "%ld", obj->d);
        break;
    case OBJ_FLOAT:
        dst = sdscatprintf(dst, "%f", obj->f);
        break;
    default:
        dst = sdscatprintf(dst, "%p", obj->ptr);
        break;
    }

    return dst;
}
